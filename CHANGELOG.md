# CHANGELOG
List of changes to `[Arduino] IROSC` between each version.

# V1.0.0

First major upgrade to code, structure & documentation.

- Added images for documentation
- Added code comments
- Added serial information for settings readout in `irosc.ino`
- Added `CHANGELOG.md`
- Updated documentation
- Updated code for ease of editing
- Updated ethernet settings in `irosc.ino`
- Updated `irosc.ino` & `irrecord.ino` to work with newer libraries (still needs update to work with the new IR libraries)
- Removed unused code from `irrecord.ino`

# v0.0.1

First version of the code.

- Added `irrecord.ino`
- Added `irosc.ino`
- Added `README.md`
