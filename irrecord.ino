#include <IRremote.h>

// VARIABLES
int RECV_PIN = 11; // sets data pin for ir receiver

IRrecv irrecv(RECV_PIN);
IRsend irsend;

decode_results results;

// SETUP FUNCTIONS
void setup() {
  Serial.begin(9600);  // opens up serial port for viewing
  irrecv.enableIRIn(); // start the receiver
}

// Storage for Recorded Code
int codeType = -1;             // type of code
unsigned long codeValue;       // code value if not raw
unsigned int rawCodes[RAWBUF]; // durations if raw
int codeLen;                   // length of the code
int toggle = 0;                // rc5/6 toggle state

// LOGGING
void storeCode(decode_results *results) {
  codeType = results->decode_type;
  int count = results->rawlen;

  // records unknown/raw ir code
  if (codeType == UNKNOWN) {
    Serial.println("Received unknown code, saving as raw");
    codeLen = results->rawlen - 1;
    for (int i = 1; i <= codeLen; i++) {
      if (i % 2) {
        // mark
        rawCodes[i - 1] = results->rawbuf[i] * USECPERTICK - MARK_EXCESS;
        Serial.print(" m");
      } else {
        // space
        rawCodes[i - 1] = results->rawbuf[i] * USECPERTICK + MARK_EXCESS;
        Serial.print(" s");
      }
      Serial.print(rawCodes[i - 1], DEC);
    }
    Serial.println("");

    // records known ir code formats
  } else {
    if (codeType == NEC) {
      Serial.print("Received NEC: ");
      // ignores repeating values
      if (results->value == REPEAT) {
        Serial.println("repeat; ignoring.");
        return;
      }
    } else if (codeType == SONY) {
      Serial.print("Received SONY: ");
    } else if (codeType == PANASONIC) {
      Serial.print("Received PANASONIC: ");
    } else if (codeType == JVC) {
      Serial.print("Received JVC: ");
    } else if (codeType == RC5) {
      Serial.print("Received RC5: ");
    } else if (codeType == RC6) {
      Serial.print("Received RC6: ");
    } else {
      Serial.print("Unexpected codeType ");
      Serial.print(codeType, DEC);
      Serial.println("");
    }
    Serial.println(results->value, HEX);
    codeValue = results->value;
    codeLen = results->bits;
  }
}

void loop() {
  if (irrecv.decode(&results)) {
    storeCode(&results);
    irrecv.resume(); // resume receiver
  }
}
