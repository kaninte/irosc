#include <Ethernet.h>
#include <EthernetUdp.h>
#include <IRremote.h>
#include <OSCBoards.h>
#include <OSCBundle.h>
#include <SPI.h>

// SETTINGS
// Ethernet Settings
EthernetUDP udp;
IPAddress ip(10, 0, 100, 1);                       // device ip
byte mac[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED}; // device mac address
const unsigned int inPort = 10000;                 // listening port

// IR Settings
IRsend irsend;
const int ledPin = 9;

// OSC FUNCTIONS
void IRStatusOn(OSCMessage &msg, int addrOffset) {
  Serial.println("[IR] ON");
  irsend.sendNEC(0xFFB04F, 32);
}
void IRStatusOff(OSCMessage &msg, int addrOffset) {
  Serial.println("[IR] OFF");
  irsend.sendNEC(0xFFF807, 32);
}

void setup() {
  Serial.begin(9600); // serial port

  Ethernet.begin(mac, ip); // start the ethernet connection
  udp.begin(inPort);       // opens osc port

  pinMode(ledPin, OUTPUT); // led pin setup
  digitalWrite(ledPin, LOW);

  // arduino status readout
  Serial.println("Connected");
  Serial.println("---------------");
  Serial.print("IP Address: ");
  Serial.println(Ethernet.localIP());
  Serial.print("OSC Port: ");
  Serial.println(inPort);
  Serial.print("IR LED Pin: ");
  Serial.println(ledPin);
  Serial.println("---------------");
}

void loop() {

  OSCBundle osc;
  int size;

  if ((size = udp.parsePacket()) > 0) {
    while (size--)
      osc.fill(udp.read());

    // OSC COMMANDS
    osc.route("/ir/status/on", IRStatusOn);
    osc.route("/ir/status/off", IRStatusOff);
  }
}
