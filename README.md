# [Arduino] IROSC

Allows you to trigger predetermined [Infrared (IR)](https://en.wikipedia.org/wiki/Infrared) signals over [OSC](http://opensoundcontrol.org/) with an [Arduino](https://www.arduino.cc/).

_Originally designed and developed on an [Arduino Mega 2560](https://store.arduino.cc/usa/mega-2560-r3) with an [Arduino Ethernet Sheild](https://www.arduino.cc/en/Main/ArduinoEthernetShieldV1)._

# Installation

1. Download the library dependancies from the original repositories down below (_make sure to get the right version number)._
2. Move the libraries into your arduino libraries folder `(~/arduino/libraries/)`.

# Library Dependancies

All library dependancies needed for compiling. Download through github for the right versions.

_Some older and newer versions may work, but only the labeled versions have been tested and verified._

**Ethernet (v2.0.0)**
https://github.com/arduino-libraries/Ethernet

**OSC (v3.5.7)**
https://github.com/CNMAT/OSC

**IRremote (v2.2.3)**
https://github.com/z3t0/Arduino-IRremote

# Instructions

**Recording IR Codes**

1. Set up arduino to record IR.

_By default, the IR decoder data pin is pin 11._

![iroscrecv](images/irosc-recv.png "IR Recv Setup")

2. Upload `irrecord.ino` to your arduino.
3. Open the serial monitor and connect to `port 9600`.
4. Press each button on the remote or IR emmiter you wish to clone/use.
5. Note down the hexcode and format for each button; this is for use in the `irosc.ino` script.

**Sending IR via OSC**

1. Set up arduino to receive OSC commands and emmit IR.

_By default, the IR LED cathode pin gets plugged into pin 9._

Note: This will need some sort of ethernet / wifi sheild. _This project has been designed for use with the [Arduino Ethernet Sheild](https://www.arduino.cc/en/Main/ArduinoEthernetShieldV1)._

![irosc](images/irosc.png "IROSC Setup")

2. Open `irosc.ino`.
3. Add the codes you recorded from `irrecord.ino` into the _OSC FUNCTIONS_.

```
void FunctionName(OSCMessage &msg, int addrOffset) {
  Serial.println("Serial Message");
  irsend.sendNEC(0xFFF807, 32);
}
```

This is the code you need to add for each IR command you wish to use. Each one must have a unique `FunctionName` which can be any string of text with no spaces or special characters. This _MUST_ match the one you use in the _OSC COMMANDS_ section.

The `Serial Message` can be whatever string of text you want as long as it's kept within the `" "`. This line isn't compulsory but is used to print out any osc commands to the serial `port 9600` to make sure the device is reciving the commands.

The `irsend.sendNEC(0xFFF807, 32);` is the outputting of the IR. This is where you paste the data from the `irrecord.ino`. The IR type is placed in the `sendNEC` part where `NEC` is the type. The HEX `0xFFF807, 32` is the IR code information where `0xFFF807` is the code itself and `32` is the bytes of data being transmitted _(the bytes of data will be fine to leave in most cases)_.

4. Add the _OSC COMMANDS_.

`osc.route("/ir/osc/address", FunctionName);`

This is the code to receive the osc command and trigger the function that sends the IR code to the LED.

The `/ir/osc/address` is the OSC address and can be whatever you want it to be (as long as it starts with a `/` and follows the osc formatting). The `FunctionName` _MUST_ match the corresponding one in the _OSC Actions_.

5. Configure Ethernet.

```
// Ethernet Settings
EthernetUDP udp;
IPAddress ip(10, 0, 100, 1);                       // device ip
byte mac[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED}; // device mac address
const unsigned int inPort = 10000;                 // listening port
```

The `device ip` and `device mac address` both have to be different to any other device on the network. You shoud be able to find the MAC Address on your arduino board. _This statically sets the ip for the arduino, if you wish to use dhcp you'll need to change some code._

The `listening port` can be whatever you wish. This is the port used to send the OSC commands to.

6. Verify and Upload to your Arduino.

You should now be able to plug your arduino into a network and send OSC commands to trigger your IR device. You can test you are reciveing commands by opening up your Serial Monitor `port 9600`.

The Serial Monitor will tell you the `IP Address` `Port Number` and `LED Pin` on startup. This can be useful if programmed and you wish to check settings without reflashing the device.
